import React from 'react';

// Creates a Context object

// A context object as the name states, it is a data type of an object that can be used to store information that can be shared to other components within the application


const UserContext = React.createContext();
//allows us to share data between components (without using props)

// Provider component - it allows other components to consume/use the context objects and supply the necessary information needed to the context object.

//provided data to components through the use of context

export const UserProvider = UserContext.Provider;


export default UserContext;