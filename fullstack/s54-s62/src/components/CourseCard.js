import { useState } from 'react';
import { Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
 
export default function CourseCard({courseProp}) {

	// console.log(props);
	// console.log(typeof props);

	const {_id, name, description, price} = courseProp;

	// Syntax
		// const [getter, setter] = useState(initialGetterValue)
	//const [count, setCount] = useState(0);
	//use the state hook for getting and setting the seats for this course
	//const [seats, setSeats] = useState(5);

	console.log(useState(0))

	// function enroll(){

	// 	if(seats>0){
	// 		setCount(count + 1);
	// 		setSeats(seats - 1);
	// 	}else{
	// 		alert("No more seats available")
	// 	}
		
	// }

	return (
		<Card>
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>{price}</Card.Text>
				<Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
			</Card.Body>
		</Card>
	)
}

// Check if the CourseCard component is getting the correct prop types
CourseCard.propTypes = {
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}
