// Import necessary dependencies from React and other libraries
import { useState, useEffect } from 'react';
import { CardGroup } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import PreviewCourses from './PreviewCourses';

// Create a functional component called FeaturedCourses
export default function FeaturedCourses() {

  // Initialize a state variable called 'previews' using useState, initially an empty array
  const [previews, setPreviews] = useState([]);

  // Use the useEffect hook to fetch data from a local server when the component mounts
  useEffect(() => {
    // Fetch data from the specified URL
    fetch(`http://localhost:4000/courses/`)
      .then((res) => res.json()) // Parse the response as JSON
      .then((data) => {
        console.log(data);

        // Initialize arrays to hold random numbers and featured course components
        const numbers = [];
        const featured = [];

        // Define a function to generate random unique numbers
        const generateRandomNums = () => {
          let randomNum = Math.floor(Math.random() * data.length);

          if (numbers.indexOf(randomNum) === -1) {
            numbers.push(randomNum);
          } else {
            generateRandomNums();
          }
        };

        // Loop through the data and generate random featured courses
        for (let i = 0; i < data.length; i++) {
          generateRandomNums();

          // Create a PreviewCourses component with course data and a unique key
          featured.push(
            <PreviewCourses data={data[numbers[i]]} key={data[numbers[i]]._id} breakPoint={2} />
          );
        }

        // Set the 'previews' state with the generated featured courses
        setPreviews(featured);
      });
  }, []); // Empty dependency array to run the effect once when the component mounts

  // Render the component with a title and a CardGroup containing the featured course components
  return (
    <>
      <h2 className="text-center">Featured Courses</h2>
      <CardGroup className="justify-content-center">{previews}</CardGroup>
    </>
  );
}
