// Syntax, Statements, and Comments


console.log('We tell the computer to log this in the console.');

// Statements - in programming are instructions that we tell the computer to perform
	// JS Statements usually end with semicolon (;)

// Syntax - it is the set of rules that describes how statements must be constructed

// For us to create a comment, we use Crtl + /
	// We have single line (crtl + /)and multi-line comments(crtl + shift + /)

	// console.log('Hello!');

	/*alert('This is an alert!');
	alert('This is another alert');*/


	// Whitespaces (spaces and line breaks) can impact functionality in many computer languages, but not in JS.

// Variables - This is used to contain data

	// Declare variables 
		// let/const/ variableName;

		let myVariable;
		console.log(myVariable); //undefined

		// variables must be declared first before they are used
		// using variables before they are declared will return an error
		let hello;
		console.log(hello);
		
		// declaring and initializing variables
		// Syntax
			// let/const variableName = initialValue;

		let productName = 'desktop computer';
		console.log(productName);

		let productPrice = 18999;
		console.log(productPrice);

		const interest = 3.539; // const variable cannot be re-assign

		// re-assinging variable values

		productName = 'Laptop';
		console.log(productName);

		let friend = 'Kate';
		console.log(friend);


