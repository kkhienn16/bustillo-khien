/*
	npm init - used to initialize a new package.json file 

	package.json - contains info about the project, dependencies, and other settings

	package-lock.json - locks the versions of all installed dependencies to its specific version
*/

const express = require("express")
const app = express();
const port = 4000;

// Middlewares
	// a software that provides common services and capabilities to application outside of what's offered by operating system.

	// app.use() is a method used to run another function or method for our express.js api
		// it is used to run middlewares

// allows our app to read json data
app.use(express.json())
// allows our app to read data from forms. this also allows us to receive information in other data types such as an object which we will use throughout our application
app.use(express.urlencoded({extended:true}))

app.get("/",(req,res)=>{
	res.send("Hello World")
})

app.get("/hello",(req,res)=>{
	res.send("Hello from /hello endpoint")
})

app.post("/hello",(req,res)=>{

	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`)
})

app.put('/',(req,res) =>{
	res.send("Hello from a put method route!")
})

app.delete('/',(req,res) =>{
	res.send("Hello from a delete method route!")
})

// array will store objects when the '/signup' route is accessed
let users = []


app.post('/signup', (req, res) => {

	console.log(req.body);

	if (req.body.username !== '' && req.body.password !== '' ) {
		users.push(req.body);
		res.send(`User ${req.body.username} has been succesfully registered`);
	} else {
		res.send("please input BOTH username and password");
	}
});

// Changes password

app.put('/change-password',(req,res) => {

	let message;

	for(let i = 0; i < users.length; i++){

		if(req.body.username == users[i].username){

			users[i].password = req.body.password;

			message = `User ${req.body.username}'s password has been updated!`

			console.log(users)
			break;
		} else {
			message = `User does not exist`
		}

	}

	res.send(message);

})

// Activity

	


// End of Activity

if(require.main == module){
	app.listen(port,() => console.log(`Server is running at port ${port}`))
}

module.exports = app;


