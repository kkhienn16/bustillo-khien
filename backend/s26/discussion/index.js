console.log("Array Traversal");

// Arrays are used to store multiple related data/values in a single variable
// for us to delcare and create arrays, we use [] also known as "array literals"


let hobbies = ['Play League' , 'Read a book' , ' Listen to Music' , 'Code'];

// Array are actually a special type of object
// does it have a key value pars? Yes. index number: element
// Arrays make it easy to manager, manipulate as set of data
// Methods is another term for functions associated with an object and is used to execute statements that are relevant to a specific object

/*
	Syntax:

	let/const arrayName = [elementA, elementB, element C . .. . .]

*/

// Common examples of arrays
let grades = [98.5 , 94.6, 98.8, 88.9];
let computerBrands = ['Acer' , 'Asus' , 'Lenovo' , 'HP' , 'Neo' , 'Redfox', 'Gateway' , 'Toshiba' , 'Fujitsu'];

// this is not recommended
let mixedArr = [12, 'Asus' , null, undefined, {}];
let arraySample = ['Cardo' , 'One Punch Man' , 25000, false];

console.log(grades);
console.log(computerBrands);
console.log(mixedArr);
console.log(arraySample);





// Mini Activity

let tasks = ['Sleep', 'Play Video Game', 'Watch Movies' , 'Code' , 'Jogging'];

let capitalCities = ['Manila', 'Beijing', 'Rome' , 'Jakarta'];

console.log(tasks);
console.log(capitalCities);

// Alternative way to write arrays

let myTasks = [
		'drink HTML',
		'eat javascript',
		'inhale css',
		'bake react'
	];

// Create an array with values from variables:

let username1 = 'gdragon22';
let username2 = 'gdino21';
let username3 = 'ladiesman217';
let username4 = 'transformers45';
let username5 = 'noobmaster68';
let username6 = 'gbutiki78';

let guildMembers = [username1, username2 , username3 , username4 , username5, username6];
console.log(guildMembers);
console.log(myTasks);

// [.length property]
	// allows us to get and set the total number of items or elements in an array.
	// the .length property of an array gives a number data type

console.log(myTasks.length); //4
console.log(capitalCities.length); //4

let blankArr = [];
console.log(blankArr.length); //0

// length property can also be used with strings

let fullName = 'Cardo Dalisay';
console.log(fullName.length);

// length property can also set the total number of items in an array, meaning we can actually delete the last item in the array or shorten the array by simply updating the length property of an array

myTasks.length = myTasks.length-1;
console.log(myTasks.length); //3
console.log(myTasks);

// to delete a specific item in an array, we can employ array methods.

// another example using decrementation
capitalCities.length--;
console.log(capitalCities);

// can we do the same with a string?
fullName.length = fullName.length-1;
console.log(fullName.length);

let theBeatles = ['John', 'Paul', 'Ringo' , 'George'];
theBeatles.length++;
console.log(theBeatles); //last element is empty
console.log(theBeatles.length);
//another way

theBeatles[4] = fullName;
console.log(theBeatles); 
//another way

theBeatles[theBeatles.length] = 'Tanggol';
console.log(theBeatles);

// [Read from Arrays]

/*
	Access elements with the use of their index
	Syntax:
	arrayName[index]

*/

console.log(capitalCities[0]); //accessed the first element of the arraySample

console.log(grades[100]); //undefined

let lakersLegends = ['Kobe', 'Shaq' ,'Lebron' , 'Magic' , 'Kareem'];
console.log(lakersLegends[2]); //3rd element
console.log(lakersLegends[4]); //5th element
// you can also save array items in another variable
let currentLaker = lakersLegends[2];
console.log(currentLaker);

console.log('Array before assignment');
console.log(lakersLegends);
lakersLegends[2] = "Pau Gasol";
console.log('Array after reassignement');
console.log(lakersLegends);

// MA 3


function findBlackMamba(index) {
 
  return lakersLegends[index];
}

let blackMamba = findBlackMamba(0);
console.log(blackMamba); //Kobe

// MA 4

let theTrainers = ['Ash'];

function addTrainers(trainer){
	theTrainers[theTrainers.length] = trainer;
}

addTrainers('Brock');
addTrainers('Misty');
console.log(theTrainers);

// Access the last element of an array
// Since the first element of an array starts at 0, subtracting 1 to the length of the array will offset the value by one allowing us to het the last element

let bullsLegends = ['Jordan' , 'Pippen', 'Rodman' , 'Rose' , 'Kukoc'];
let lastElementIndex = bullsLegends.length-1;

console.log(bullsLegends[lastElementIndex]);
console.log(bullsLegends[bullsLegends.length-1]);


// Add items into the array

let newArr = [];
console.log(newArr[0]);

newArr[0] = "Cloud Strife";
console.log(newArr);

newArr[1] = "Tifa Lockhart";
console.log(newArr);

newArr[newArr.length] = "Barret Wallace";
console.log(newArr);

newArr[newArr.length-1] = "Aerith Gainsborought";
console.log(newArr);


// [Loop over an array]

for(let index = 0; index < newArr.length; index++){
	console.log(newArr[index]);
};


let numArr = [5,12,30,48,40];

for (let index = 0; index < numArr.length; index++){

	if(numArr[index] % 5 ===0){
		console.log(numArr[index] + " is divisible by 5!");

	} else{
		console.log(numArr[index] + " is not divisible by 5!");
	}
};

// [Multidimensional Arrays]

let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];

console.log(chessBoard);

// access elements of a multidimensional Array
console.log(chessBoard[1][4]); //e2
console.log("Pawn moves to: " + chessBoard[1][5]);//f2

//a8
console.log(chessBoard[7][0]);
//h6
console.log(chessBoard[5][7]);
//a1
console.log(chessBoard[0][0]);
