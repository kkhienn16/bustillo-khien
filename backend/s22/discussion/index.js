console.log("Hello!, B297");

// functions
	// Parameters and arguments

	/*function printName (){

		let nickname = prompt("Enter your nickname: ");
		console.log("Hi, " + nickname); 
	}

	printName();*/


function printName (name){
		
		console.log("Hi, " + name); 
	}

printName("Baken");

let sampleVariable = "Cardo";

printName(sampleVariable);


function checkDivisibilityBy8(num){

	let remainder = num % 8;
	console.log (`The remainder of ${num} divided by 8 is: ${remainder}`);
	let isDivisibleBy8 = remainder === 0;
	console.log(`Is ${num} divisible by 8?`);
	console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(28);
checkDivisibilityBy8(9678);

/*
	Mini-activity
	check the divisibility of a number by 4
	have one parameter named num

	1.56
	2.95
	3.444

*/

function checkDivisibilityBy4(num){

	let remainder1 = num % 4;
	console.log (`The remainder of ${num} divided by 4 is: ${remainder1}`);
	let isDivisibleBy4 = remainder1 === 0;
	console.log(`Is ${num} divisible by 4?`);
	console.log(isDivisibleBy4);
}

checkDivisibilityBy4(56);
checkDivisibilityBy4(95);
checkDivisibilityBy4(444);

// Functions as arguments 
// Function parameters can also accept other functions as arguments

function argumentFunction(){
	console.log("This function was passed as an argument before the message was printed");
};

function invokeFunction(argumentFunction) {
	argumentFunction();
};

// Adding and removing parenthesis "()" impacts the outout of JS heavily
// when a function is used with parenthesis "()", it denotes invoking/calling a function
// A function used without a parenthesis is normally assiociated with using the function as an argument to another function

invokeFunction(argumentFunction);

// Using multiple parameters 

function createFullName (firstName, middleName, lastName){

	console.log(`${firstName} ${middleName} ${lastName}`);
};

createFullName("Khien", "Ebon", "Bustillo");
createFullName("Khien", "Ebon");
createFullName("Khien", "Ebon", "Bustillo" , "Jr.");


// Using variables as arguments

let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

createFullName(firstName, middleName, lastName);

// Miniactivity2

function printFriends(friend1, friend2,friend3) {
	console.log(`My three friends are: ${friend1}, ${friend2}, ${friend3}`);
}

printFriends('Rhica' , 'Marc' , 'Daniel');

// Return statement

function returnFullName(firstName,middleName,lastName){
	return firstName + ' ' + middleName + ' ' +lastName;
	console.log("This will not be printed!");
}

let completeName1 = returnFullName("Monkey" , "D" , "Luffy");
let completeName2 = returnFullName("Cardo" , "Tanggol" , "Dalisay");

console.log(completeName1 + " is my bestfriend!");
console.log(completeName2 + " is my friend!");



//1
function getSquareArea(side){
	return side ** 2;
}

let areaSquare = getSquareArea(4);
console.log("The area of a square with a length of 4");
console.log(areaSquare);

//2

function computeSum (x,y,z){
	return x + y + z;
}


let sumOfThreeDigits = computeSum(1,2,36);
console.log("The sum of three numbers are: ")
console.log(sumOfThreeDigits);

//3

function compareToOneHundred (num){
	return num === 100;

}

let booleanValue = compareToOneHundred(99);
console.log("is this one hundred?");
console.log(booleanValue);