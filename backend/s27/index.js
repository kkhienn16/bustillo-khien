console.log('Array Manipulation!');

// Array Methods 
	// JS has built-in functions and methods for arrays. allows us to manipulate and access array items

	// Mutator Methods

	/*
		are functions that mutate or change an array after they are created
		these methods manipulate the original array performing barious tasks suck as adding and removing elements
	
	*/

	// array
	let fruits = ['Apple' , 'Orange' , 'Kiwi' , "Dragon Fruit"];

	console.log(fruits);

// push()
	// Adds an element in the END of an array AND returns the array's LENGTH
	// Syntax:
		// arrayName.push("itemToPush");

		console.log('Current array:');
		console.log(fruits);

		// if we assign it to a variable, we are able to save the length of the array
		let fruitsLength = fruits.push("Mango");
		console.log(fruitsLength);
		console.log('Mutated array from push method:');
		console.log(fruits);

// pop()
		// REMOVES the LAST element in an array AND returns the removed element

		// Syntax
			// arrayName.pop();

		let removedFruit = fruits.pop();
		console.log(removedFruit);
		console.log('Mutated array  from pop method:');
		console.log(fruits);

// Mini Activity
		// function called removeFruit
		// deletes the last item in the array
		// after invoking the removeFruit function. log the fruits in the array console.

		function removeFruit(){
			fruits.pop();
		}

		removeFruit();
		console.log(fruits); //deleted dragon fruit

// unshift()
		// adds one or more elements at the beggining of an array
		// Syntax
			// - arrayName.unshift('elementA');
			// - arrayName.unshift('elementA', 'elementB');

		fruits.unshift('Lime', 'Banana'); //added lime and banana in the beggining
		console.log('Mutated array from unshift method');
		console.log(fruits);

		function unshiftFruit(fruit1, fruit2){
			fruits.unshift(fruit1 , fruit2);
		}

		unshiftFruit('Grapes', "Papaya");
		console.log(fruits);

// shift()
		// removes the first element of an array and returns the removed element
		// arrayName.shift();

		let anotherFruit = fruits.shift();
		console.log(anotherFruit);
		console.log('Mutated Array from the shift method:');
		console.log(fruits);

// splice()
		// simultanously removes elements from a specified index number and adds elements.

		// syntax
			// arrayName.splice(startingIndex, dtnseleteCount,elemeToBeAdded);

		fruits.splice(1, 2, 'Calamansi', 'Cherry');
		console.log('Mutated Array from the splice method:');
		console.log(fruits);


		function spliceFruit(index,deleteCount, fruit){
			fruits.splice(index, deleteCount , fruit);
		};

		spliceFruit(1,1,"Avocado");
		spliceFruit(2,1, "Durian");
		console.log(fruits);

// sort()
		// re-arranges the array elements in alphanumeric order (A-Z)
		// Syntax
			// arrayName.sort()

			fruits.sort();
			console.log('Mutated array from sort method:');
			console.log(fruits);


//reverve()
	// reverses the order of array elements(Z-A)
	// syntax
			// arrayName.reverse();

			fruits.reverse();
			console.log('Mutated array from reverse method:');
			console.log(fruits);



