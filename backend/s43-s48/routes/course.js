// Route
// Dependencies and Modules
const express = require("express");
const courseController = require("../controllers/course");
const auth = require("../auth");
const { verify, verifyAdmin } = auth;

 //Routing Component
const router = express.Router();

//create a course POST
router.post("/",verify,verifyAdmin,courseController.addCourse);

// route for retrieving all courses
router.get("/all",courseController.getAllCourses);

// create a route for getting all active courses
// use default endpoint
// getAllActiveCourses

router.get("/",courseController.getAllActiveCourses);

// get a specific course
router.get("/:courseId",courseController.getCourse);

// edit a specific course
router.put("/:courseId",verify, verifyAdmin,courseController.updateCourse);

// activity
	// archive
router.put("/:courseId/archive",verify, verifyAdmin,courseController.archiveCourse);
	// activate
router.put("/:courseId/activate",verify, verifyAdmin,courseController.activateCourse);

// Route for searching courses by name
router.post('/search', courseController.searchCoursesByName);

// Route to get all enrolled user to a certain course
router.get('/:courseId/enrolled-users',courseController.getEmailsOfEnrolledUsers);

// Route to search for courses by price range
router.post('/search-by-price', courseController.searchCoursesByPrice);



// Export Route System
module.exports = router;