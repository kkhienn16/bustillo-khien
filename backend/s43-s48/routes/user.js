// Route
// Dependencies and Modules
const express = require("express");
const userController = require("../controllers/user");
const auth = require("../auth");
const { verify, verifyAdmin } = auth;

 //Routing Component
const router = express.Router();


// Routes - POST
// check email
router.post("/checkEmail",(req,res) => {
	userController.checkEmailExist(req.body).then(resultFromController => res.send(resultFromController))
});

// Register user
router.post("/register",(req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

// User Authentication
router.post("/login",userController.loginUser);

// details
router.post("/details", verify, userController.getProfile);


//Route to enroll user to a course

router.post("/enroll",verify ,userController.enroll);

//Get Logged User's Enrollments

router.get("/getEnrollments",verify, userController.getEnrollments);

// POST route for resetting the password
router.post('/reset-password', verify, userController.resetPassword);

// Update user profile route
router.put('/profile', verify, userController.updateProfile);


// Export Route System
module.exports = router;