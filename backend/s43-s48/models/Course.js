// Dependencies
const mongoose = require("mongoose");

// Schema/Blueprint

	// course model

const courseSchema = new mongoose.Schema({
	
	name: {
		type: String,
		required: [true,"course is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	isActive: {
		type: Boolean,
	    default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	enrollees: [
			{
				userId: {
					type: String,
					required: [true, "UserId is required"]
				},
				enrolledOn:{
					type: Date,
					default: new Date()
				}
			}
		]
})



// Model
module.exports = mongoose.model("Course",courseSchema);



