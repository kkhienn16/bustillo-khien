// Activity

// Dependencies
const mongoose = require("mongoose");



	// User model
const userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: [true,"First name is required"]
	},
	lastName: {
		type: String,
		required: [true,"Last name is required"]
	},
	email: {
		type: String,
		required: [true,"Email is required"]
	},
	password: {
		type: String,
		required: [true,"Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		required: [true,"Moblie Number is required"]
	},
	enrollments: [
			{
				courseId:{
					type: String,
					required: [true,"Last name is required"]
				},
				enrolledOn: {
					type: Date,
					default: new Date()
				},
				status: {
					type: String,
					default: "Enrolled"
				}

			}
		]
})

// Model

module.exports = mongoose.model("User",userSchema);