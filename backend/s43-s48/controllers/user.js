// controller
const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Check if the email exist already
/*
	Steps:
	1. "find" mongoose method to find duplicate items
	2. "then" method to send a response back to the FE applicaiton based of the result of the "find" method
*/

module.exports.checkEmailExist = (reqbody) => {

	return User.find({email: reqbody.email}).then(result => {

		// find a method returns a record if a match is found
		if(result.length > 0) {
			return true;
		}
		// no duplicate found
		// the user is not yet registered in the db
		else {
			return false;
		}
	})

}


// User Registration
/*
	Steps:
	1. create a new user object
	2. make sure that the password is encrypted
	3. save the new user to the database
*/
module.exports.registerUser = (reqbody) => {
	
	let newUser = new User({

		firstName: reqbody.firstName,
		lastName: reqbody.lastName,
		email: reqbody.email,
		mobileNo: reqbody.mobileNo,
		password: bcrypt.hashSync(reqbody.password,10)
	})

	return newUser.save().then((user,error) => {

		if(error){
			return false;
		}else {
			return true;
		}
	})
	.catch(error => err)
}

// User authentication

/*
	Steps:
	1. check the db is user email exist
	2. compare the password from req.body and password in the db
	3. Generate a JWT if the user logged in and return false if not 
*/

module.exports.loginUser = (req,res) => {

	return User.findOne({email: req.body.email}).then(result =>{
		if(result === null){
			return false;
		}else {
			// compareSync method is used to compare a non-encrypted password and from the encrypted password from the db
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password)

			// if pw match
			if(isPasswordCorrect){
				return res.send({access: auth.createAccessToken(result)})
			}
			// else pw does not match
			else {
				return res.send(false);
			}
		}
	})
	.catch(err => res.send(err))
}

// details
module.exports.getProfile = (req,res) =>{
	
	return User.findById(req.user.id).then(result=>{
			result.password = "";
			return res.send(result)
	})
	.catch(err => res.send(err))
};

// Enroll user to a class
/*
	Steps:
	1. Find the document in the database using the user's ID 
	2. Add the course ID to the user's enrollment array
	3. Update the document in the MongoDB Atlas Database
*/
module.exports.enroll = async (req,res) => {

	// to check in the terminal the user id adn courseId
	console.log(req.user.id);
	console.log(req.body.courseId);

	// check if the user is an admin and deny the enrollment
	if(req.user.isAdmin){
		return res.send("Action Forbidden")
	}

	// Creates an "isUserUpdated" variable and returns true upon sucessful update otherwise returns error
	let isUserUpdated = await User.findById(req.user.id).then(user => {

		// Add the courseId in an object and push that object into the user's "enrollment" array
		let newEnrollment = {
			courseId: req.body.courseId
		}

		// Add the course in the "enrollments" array
		user.enrollments.push(newEnrollment);

		// Save the updated user and return true if successful or the error message if failed.
		return user.save().then(user => true).catch(err => err.message);

	});

	// Checks if there are error in updating the user
	if(isUserUpdated !== true){
		return res.send({message: isUserUpdated});
	}


	
	let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {

		let enrollee = {
			userId: req.user.id
		}

		course.enrollees.push(enrollee);

		return course.save().then(course => true).catch(err => err.message);
	})

	// checks if there are error in updating the course
	if(isCourseUpdated !== true){
		return res.send({message: isCourseUpdated});
	}


	// checks if both user update and course update are successful
	if(isUserUpdated && isCourseUpdated){
		return res.send({message: "Enrolled Successfully."});
	}
}


// Activity

module.exports.getEnrollments = (req,res) => {
	
	User.findById(req.user.id).then(result => 
	res.send(result.enrollments)).catch(error => err)

}

// Function to reset the password
module.exports.resetPassword = async (req, res) => {
  try {

  	// used object destructuring
    const { newPassword } = req.body;
    const { id } = req.user; // Extracting user ID from the authorization header

    // Hashing the new password
    const hashedPassword = await bcrypt.hash(newPassword, 10);

    // Updating the user's password in the database
    await User.findByIdAndUpdate(id, { password: hashedPassword });

    // Sending a success response
    res.status(200).json({ message: 'Password reset successfully' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  }
};


// Controller function to update the user profile
module.exports.updateProfile = async (req, res) => {
  try {
    // Get the user ID from the authenticated token
    const userId = req.user.id;

    // Retrieve the updated profile information from the request body
    const { firstName, lastName, mobileNo } = req.body;
      
    // Update the user's profile in the database
    const updatedUser = await User.findByIdAndUpdate(
      userId,
      { firstName, lastName, mobileNo },
      { new: true }
    );

    res.json(updatedUser);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Failed to update profile' });
  }
}

// activity
module.exports.searchCoursesByPrice = async (req, res) => {
  try {
    const { minPrice, maxPrice } = req.body;

    // Query courses within the given price range
    const courses = await Course.find({
      price: { $gte: minPrice, $lte: maxPrice }
    });

    res.json(courses);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

