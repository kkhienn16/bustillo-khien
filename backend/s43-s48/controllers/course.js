// controller
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth")
const User = require("../models/User")

module.exports.addCourse = (req,res) => {
	
	let newCourse = new Course ({
		name : req.body.name,
		description : req.body.description,
		price : req.body.price
	})

	// saves the created object to our database
	return newCourse.save().then((course,error) => {

		//  Course creation failed
		if(error){
			return res.send(false)

		// Course creation successful
		} else {
			return res.send(true)
		}
	})
	.catch(err => res.send(err))
};


// Retrieve all courses

/*
	1.Reatrieve all the courses from the database
*/

// We will use the find() method for our course model

module.exports.getAllCourses = (req,res) => {
	return Course.find({}).then(result =>{
		return res.send(result)
	})
}

// getAllActiveCourses
// create a function that will handle req,res
// that will find all active courses in the db

module.exports.getAllActiveCourses = (req,res) => {
	return Course.find({isActive:true}).then(result => {
		return res.send(result)
	})
}

// Get a specific course
module.exports.getCourse = (req,res) => {
	return Course.findById(req.params.courseId).then(result => {
		return res.send(result)
	})
}

// updating a course

module.exports.updateCourse = (req,res) => {

	let updatedCourse = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	return Course.findByIdAndUpdate(req.params.courseId, updatedCourse).then((course,error) =>{
		if(error){
			return res.send(false);
		}else {
			return res.send(true);
		}
	})
}

// activity
	// archive
module.exports.archiveCourse = (req,res) => {
	let archive = {
		isActive: req.body.isActive
	}
	return Course.findByIdAndUpdate(req.params.courseId, archive).then((course,error) => {
		if(error){
			return res.send(false);
		} else {
			return res.send(true);
		}
	})

} 

	// activate
module.exports.activateCourse = (req,res) => {
		let archive = {
		isActive: req.body.isActive
	}

	return Course.findByIdAndUpdate(req.params.courseId,archive).then((course,error)=> {
		if(error){
			return res.send(false);
		}else {
			return res.send(true)
		}
	})

} 

// Controller action to search for courses by course name
module.exports.searchCoursesByName = async (req, res) => {
  try {
    const { courseName } = req.body;

    // Use a regular expression to perform a case-insensitive search
    const courses = await Course.find({
      name: { $regex: courseName, $options: 'i' }
    });

    res.json(courses);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

// Controller action to get emails of enrolled users
module.exports.getEmailsOfEnrolledUsers = async (req, res) => {
  const courseId = req.params.courseId; // Use req.params instead of req.body to get the courseId from the URL parameter

  try {
    // Find the course by courseId
    const course = await Course.findById(courseId);

    if (!course) {
      return res.status(404).json({ message: 'Course not found' });
    }

    // Get the userIds of enrolled users from the course
    const userIds = course.enrollees.map(enrollee => enrollee.userId);

    // Find the users with matching userIds
    const enrolledUsers = await User.find({ _id: { $in: userIds } }); // Use userIds instead of 'users'

    // Extract the emails from the enrolled users
    const emails = enrolledUsers.map(user => user.email); // Use map() instead of forEach()

    res.status(200).json({ userEmails: emails }); // Correct the object property name

  } catch (error) {
    res.status(500).json({ message: 'An error occurred while retrieving enrolled users' });
  }
};


// activity
// Controller action to search for courses by price range
module.exports.searchCoursesByPrice = async (req, res) => {
  try {
    const { minPrice, maxPrice } = req.body;

    // Query courses within the given price range
    const courses = await Course.find({
      price: { $gte: minPrice, $lte: maxPrice }
    });

    res.json(courses);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};



