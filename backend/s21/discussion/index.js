console.log("Hi, B297!");


// Mini-Activity - Log your favorite movie line 20 times in the console

function printLine(){
	console.log("Wewewe");
};
printLine(); //calling or invoking function

//Functions 
	// lines/block of code that tellour devices to perform a certain task when called/invoked

// Function declaration

	/*
		Syntax:

		function functionName() {
			code block (statement)
		}

	*/
	//function decleration
	function printName (){
		console.log("My name is Khien.");
	};

	//function invocation
	printName();


	declaredFunction();

//Function decleration vs expressions

	//function decleration
		//function decleration is created with the function keyword and adding a function name
	// The are "saved for later use"
	// can be hoisted

	function declaredFunction() {
		console.log("Hello from declaredFunction!")
	};

	declaredFunction();

	// function expression
		// function expression is stored in a variable
		// function expression is an anonymous function assigned to the variable function
		// cannot be hoisted

	// variableFunction();

	let variableFunction = function() {
		console.log("Hello from function expression!");
	};

	variableFunction();

	// a function expression of function named funcName assigned to the variable funcExpression

	let funcExpression = function funcName() {
		console.log("Hello from the other side!");

	};

	funcExpression();

	// We can also reassign declared functions and function expressions to new anonymous functions

	declaredFunction = function() {
		console.log("updated declaredFunction");
	};

	declaredFunction();

	funcExpression = function() {
		console.log("updated funcExpression");
	};

	funcExpression();

	const constantFunc = function(){
		console.log("Initialized with const!");
	};

	constantFunc();

	/*constantFunc = function () {
		console.log("Cannot be reassigned!");
	};

	constantFunc();*/

	// Function Scooping

	/*
		Scope - acessibility/visibility of variables 

		JS Variables has 3 types of scope:
		1. local/block scope
		2. global scope
		3. function scope

	*/

	/*	// local/blockscope
		{
			let a = 1;
		}

		//global scope
		let a = 1;

		//function scope
		function sample(){
			let a = 1;
		}
		*/

	{
		let localVar = "Armando Perez";
	}

	let globalVar = "Mr. Worldwide";

	// console.log(localVar); // result an error you cant call the local variable outside its block

	console.log(globalVar);

	function showNames(){

		var functionVar = "Joe";
		const functionConst = "John";
		let functionLet = "Jane";

		console.log(functionVar);
		console.log(functionConst);
		console.log(functionLet);
	};

	showNames();

	
	// Nested Functions

	function myNewFunction() {

		let name = "Jane";

		function nestedFunction() {
			let nestedName = "John";
			console.log(name);
		}

		nestedFunction();
	}	

	myNewFunction();

	// Function and Global Scoped Variables

	// Global Scoped Variable

	let globalName = "Cardo";

	function myNewFunction2() {
		let nameInside = "Hillary";
		console.log(globalName);
	}

	myNewFunction2();


	// Using alert()

	// alert("This will run immediately when the page loads.");

	function showSampleAlert (){
		alert("Hello, Earthlings! This is from a function!");
	}

	// showSampleAlert();

	console.log("I will only log in the console when the alert is dismmissed!");
	// Using prompt()

	// let samplePrompt = prompt("Enter your Name: ");

	// console.log("Hi, I am " + samplePrompt);

	// prompt returns an empty string when there is no input. or null if the user cancels the prompt()

	function printWelcomeMessage()
	{
		let firstName = prompt("Enter your firstName: ");
		let lastName = prompt("Enter your lastName: ");

		console.log("Hello, " + firstName + " " + lastName + "!")
		console.log("Welcome to my page!");
	};

	// printWelcomeMessage();

	// The Return Statement

	/*
		The return statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function
	*/

	function returnFullName() {
		return "Jeffrey" + ' ' + "Smith" + ' ' + "Bezos";
		console.log("This message will not be printed!");
	}

	let fullName = returnFullName();
	console.log(fullName);

	function returnFullAddress() {
		let fullAddress = {
			street : "#44 Maharlika St.",
			city: "Cainta",
			province: "Rizal"
		};

		return fullAddress;
	}

	let myAddress = returnFullAddress();
	console.log(myAddress);


	function printPlayerInfo() {

		console.log("Username: " + "dark_magician");
		console.log("Level: " + 95);
		console.log("Job: " + "Mage");
	}

	let user1 = printPlayerInfo();
	console.log(user1); // undefined

	function returnSumof5and10() {
		return 5 + 10;
	}

	let sumOf5And10 = returnSumof5and10(); //15
	console.log(sumOf5And10); 

	let total = 100 + returnSumof5and10(); //115
	console.log(total);

	function getGuildMembers() {
		return ["lulu" , "Tristana", "Teemo"];

	}

	console.log(getGuildMembers);

	// Function Naming Conventions

	function getCourses() {
		let courses = ["ReactJs" , "ExpressJs 101", "MongoDB 101"];
		return courses;
	}

	let courses = getCourses();
	console.log(courses);

	// aviod using generic names and pointless and inappropriate function names
	function get (){
		let color = "pink";
		return name;
	}

	// use camelCase
	function displayCarInfo() {
		console.log("Brand: Toyota");
		console.log("Type: Sedan");
	}

	displayCarInfo();

	// activity s21

	function getUserInfo (){

		return {
			name: "John Doe",
			age: 25,
			address: "123 Street, Quezon City ",
			isMarried: false,
			printName: "Danny"
		}
	}

	let userInfo = getUserInfo();
	console.log(userInfo);