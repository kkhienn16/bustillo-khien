console.log("Intro to JSON!")

/*
	JSON
		JSON stands for JS Object Notation
		JSON is also used in other progeramming languages

		Syntax:

		{
			"propertyA": "valueA",
			"propertyB": "valueB"

		}

		JSON are wrapped in curly braces
		Properties/keys are wrapped in double qoutations

*/

	let sample1 = `

		{
			"name": "Jayson Derulo",
			"age": 20,
			"address": {
				"city": "Quezon City",
				"country": "Philippines"
			}

		}

	`;

	console.log(sample1); 
	console.log(typeof sample1); //string

	// Are we able to turn a JSON into a JS Object? - YES

	console.log(JSON.parse(sample1));


	// JSON Array 
		// JSON Array is an array of JSON

	let sampleArr = `

			[
				{
					"email":"wackywizard@example.com",
					"password":"laughOutLoud123",
					"isAdmin":true 
				},
				{
					"email":"dalisay.cardo@gmail.com",
					"password":"dalisay.cardo@gmail.com ",
					"isAdmin":true 
				},
				{
					"email":"lebroan4goat@ex.com",
					"password":"ginisamix",
					"isAdmin":false
				},
				{
					"email": "ilovejson@gmail.com",
					"password": "jsondontloveme",
					"isAdmin":false
				}
			]
	`;


	// Can we use Array Methods on a JSON Array? - No. because a JSON is a string

	// So what can we do to be able to add more items/object into our sampleArr?
		//PARSE the JSON array to a JS Array and save it in a variable

	let parsedSampleArray = JSON.parse(sampleArr);
	console.log(parsedSampleArray);
	console.log(typeof parsedSampleArray);


	console.log(parsedSampleArray.pop());
	console.log(parsedSampleArray);

	// if for example,we need to send this data back to our client/frontend, it should be in JSON format

	// JSON.parse() does not mutate or update the orginal JSON
		// we can actually turn a JS Object into a JSON again

	// JSON.stringify() - thi will strigify JS objects as JSON

	sampleArr = JSON.stringify(parsedSampleArray);

	console.log(sampleArr);
	console.log(typeof sampleArr); // string

	// Database (JSON) => Server/API  (JSON to JS Object to process) => sent as JSON  => Frontend/client


	let jsonArr = `

		[
			"pizza",
			"hamburger",
			"spaghetti",
			"hotdog stick on a pineapple",
			"pancit bihon"
		]
	`;
	
	/*
		MA: Given the json array, process it and convert it to a JS object so we can manipulate the array
		-Delete the last item in the array and a new item in the array
		-Stringify the array back into JSON
		- and update/re-assign jsonArr with the stringified array
		-log the value of jsonArr in the console, send a screencap of your console.
	*/

	let parsedJsonArr = JSON.parse(jsonArr);

	parsedJsonArr.pop();
	parsedJsonArr.push("fries");

	jsonArr = JSON.stringify(parsedJsonArr);
	console.log(jsonArr);

	// Gather User Details

	let firstName = prompt("What is your first name?");
	let lastName = prompt("What is your last name?");
	let age = prompt("What is your age?");
	let address = {
		city: prompt("which city do you live in?"),
		country: prompt("which country does your city address belong to?")

	};

	let otherData = JSON.stringify({
		firstName: firstName,
		lastName: lastName,
		age: age,
		address: address
	});

	console.log(otherData);

	