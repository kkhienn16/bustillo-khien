console.log('Array Non-Mutator and Iterator Methods');

/*	
	Non-Mutator Methods
		-Non-Mutator methods are functions that do not modify or chaange an array after they are created
		- Do not manipulate the original array performing various tasks such as:
		-returning elements from an array
		-combining arrays
		-printing the output

*/

let countries = ['US' , 'PH' , 'CAN' , 'SG' , 'TH', 'PH', 'FR', 'DE'];

// indexOf()
/*
	- Returns the index number of the first matching element found in an array

	-if no match is found, the result will be -1
*/

	let firstIndex = countries.indexOf('PH');
	console.log(`Resutlt of indexOf Method: ${firstIndex}`); //1

	let invalidCountry = countries.indexOf('BR');
	console.log('Result of indexOf Method: ' + invalidCountry); //-1

// lastIndexOf
/*
	Returns the index number of the last matching element found in the array
*/

	let lastIndex = countries.lastIndexOf('PH');
	console.log(`Result of lastIndexOf: ${lastIndex}`);

	let lastIndexStart = countries.lastIndexOf('PH',6);
	console.log('Result of lastIndexOf method: ' + lastIndexStart);


// slice()
	/*
		Portions/slices elements from an array AND returns a new array
	*/

console.log(countries);

let sliceArrayA = countries.slice(2);
console.log(`Result from slice mehtod:`);
console.log(sliceArrayA);

let slicedArrayB = countries.slice(2,4);
console.log(slicedArrayB);

let sliceArrayC = countries.slice(-3);
console.log('Result from a slice method:');
console.log(sliceArrayC);	

// toString()
/*
	Returns an array as a string separated by commas
*/

let stringArray = countries.toString();
console.log('Result from toString method: ');
console.log(stringArray)

let sampArr = [1,2,3];
console.log(sampArr.toString());

// concat()
/*
	combines two arrays and returns the combined result
*/

let taskArrayA = ['drink html' , 'eat javascript'];
let taskArrayB = ['inhale css' , 'breath mongodb'];
let taskArrayC = ['get git' , 'be node'];

let tasks = taskArrayA.concat(taskArrayB);
console.log('Result from concat method: ');
console.log(tasks);

console.log('Result from concat method:');
let allTasks = taskArrayA.concat(taskArrayB,taskArrayC);
console.log(allTasks);


let combinedTasks = taskArrayA.concat('smell express', 'throw react');

console.log('Result from concat method');
console.log(combinedTasks);

// join()

/*
	Returns an array as a string separated by specified separator (string data type)

*/

let users = ['John' , 'Jane', 'Joe', 'James'];
console.log(users.join()); // default
console.log(users.join(''));
console.log(users.join(' - '));


// Iteration methods

/*
	-These are loops designed to perform repetitive tasks on arrays 

	-Array Iteration methods normally work with a function supplied as an argument

*/

// forEach()
/*
	- Similar to a for loop that iterates on each array element

	- for each item in the array, the anonymous function passed in the forEach() method will be run

	- The anonymous function is able to receive the current item being iterated or loop over by assigning a PARAMETER

	-Variable names for arrays are normally written in the plural form

	-It is a common practice to use singular form of the array content for the parameter names used in array loops

	-forEach() does not return anything
*/

console.log(allTasks);

	allTasks.forEach(function(task){
		console.log(task);
	});

	let filteredTasks =[];

	allTasks.forEach(function(task){
		
		// console.log(task);
		if(task.length > 10){

			filteredTasks.push(task);
		}


	});

	console.log("Result of filteredTasks: ");
	console.log(filteredTasks);

	// map()
	/*
		Iterates on each element AND returns new array with different values depending on the result of the function's operation

		We require the use of a "Return"
	*/

		let numbers = [ 1,2,3,4,5 ];

		let numberMap = numbers.map(function(number){
			return number * number;
		})

		console.log('Original Array: ');
		console.log(numbers);
		console.log('Result of map method: ');
		console.log(numberMap);

		// map() vs forEach()

		let numberForEach = numbers.forEach(function(number){
			return number * number;
		})

		console.log(numberForEach);

// every()

/*
	Checks if all elements in an array meet a given condition

*/

let allValid = numbers.every(function(number){
	return (number < 3);
	// return (are all numbers less than 3)
})

console.log('Result of every method:');
console.log(allValid); //false 

// some()

/*
	Checks if at least one element in the array meets the given condition
*/

let someValid = numbers.some(function(number){
	return (number < 2);
	// return (are some numbers less than 2?)
})

console.log('Result of some method');
console.log(someValid); // true

// filter()

/*
	Returns a new array that contains elements which meets the given condition

	Returns an empty array if no elements were found

*/

let filterValid = numbers.filter(function(number){
	return(number < 3);
	// return numbers that are less than 3
})

console.log("Result of filter mehtod:");
console.log(filterValid);

let nothingFound = numbers.filter(function(number){
	return (number = 0);
	// return numbers that are equal to zero
})

console.log('Result of filter method')
console.log(nothingFound);

// Mini Activity 1



let forEachFilter = [];

numbers.forEach(function(number){
	if (number < 3) {
		forEachFilter.push(number)
	}

	
})

console.log('Result of forEach method:');
console.log(forEachFilter);


let products = ['Mouse' , 'Keyboard' , 'Laptop' , 'Monitor'];

// includes()

/*
	checks if the argument passed can be found in the array

	- returns a boolean value
*/

let productFound1 = products.includes("Mouse");
console.log(productFound1);

let productFound2 = products.includes("Headset");
console.log(productFound2);

/*
	Mrthods can be "chained" using them one after another
*/

let filteredProducts = products.filter(function(product){

	return product.toLowerCase().includes('a');
})

console.log(filteredProducts);

// reduce()
/*
	Evaluates elements from left to right and returns/reduces the array into a single value

	- "accumulator" parameter in the function sotres the result for every iteration of the loop

	- "current value" is the current element in the array that is evaluated in each iteration of the loop

	Process:
	1. first/result element in the array is stored in the "Accumulator"

	2. second/next element in the array is stored in the current value

	3. an operation is performed on the 2 elements 

	4.the loop repeats the whole step 1-3 until all elements have been worked on
*/

console.log(numbers); // [1, 2, 3, 4, 5]
						// 1 - accumulator
					  //1 + 2 = 3 (3 is the accumulator)
					  //3 + 3 = 6 (4 is the accumulator)
					  //6 + 4 = 10 (10 is the accumulator)
					  //10 + 5 = 15 

let iteration = 0;
let reducedArray = numbers.reduce(function(x,y){
	// use to track the current iteration count and accumulator/currentValue data
	console.warn('Current iteration :' + ++iteration);
	console.log('accumulator: ' + x);
	console.log('current value: ' + y);

	return x + y;

})

console.log('Result of reduce method: ' + reducedArray);
console.log(reducedArray);


// reduce string arrays to string
let list = ['Hello' , 'From' , 'The' , 'Other' , 'Side'];
let reducedJoin = list.reduce(function(x,y){

	console.log(x);
	console.log(y);
	return x + ' ' + y;
})

console.log("Result of reduce method: " + reducedJoin);