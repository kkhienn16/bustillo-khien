console.log("Hello World!");

// Arithmetic Operators
// +, -, *, /, % (modulo operator)
	
	let x = 1397;
	let y = 7831;

	let sum = x + y;
	console.log("Result of additiion operator:" + sum);

	// Mini-Activity

	// difference
	let difference = x - y;
	console.log("Result of subtraction operator:" +difference)
	// product
	let product = x * y;
	console.log("Result of multiplication operator:" +product)	
	// qoutient
	let qoutient = x / y;
	console.log("Result of modulo operator:" + qoutient)
	// remainder
	let remainder = x % y;
	console.log("Result of division operator:" + remainder)

	let a = 10;
	let b = 5;

	let remainderA = a % b;
	console.log(remainderA); //0

// Assignment Operator
	// Basic Assignment operator (=)

let assignmentNumber = 8;

// Addition Assignment Operator
assignmentNumber = assignmentNumber + 2;
assignmentNumber += 2;
console.log(assignmentNumber);


// Subtraction/Multiplication/Division Assignment operator

assignmentNumber -= 2; //10
console.log("Result of -= :" + assignmentNumber);

assignmentNumber *= 2;
console.log("Result of *= :" + assignmentNumber);

assignmentNumber /= 2;
console.log("Result of /= :" + assignmentNumber);

// Multiple Operators and Parenthesis

/*
	1. 3 * 4 = 12;
	2. 12 / 5 = 2.4;
	3. 1 + 2 = 3
	4. 3 - 2.4 = 0.6
*/
let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas);

/*
	1. 4/5 = 0.8
	2. 2-3 = -1
	3. -1 * 0.8 = -0.8
	4. 1 + -0.8 = 0.2
*/
let pemdas = 1 + (2-3) * (4/5);
console.log(pemdas);

// Increment and Decrement
	// Operators that add or subtract values by 1 and re-assigns the value of the variable where the increment/decrement was applied to 

	let z = 1;
	let increment = ++z;

	// the value of "z" is added by a value of one before returning the value and storing it in the variable "increment".

	// increase and re-assign

	console.log("Result of pre-increment: " + increment);
	console.log("Result of pre-increment: " + z);

	// the value of z is returned and stored in the variable "increment" then the value if z is increased by one

	// re-assign then increase

	increment = z++;
	console.log("Result of post-increment: " + increment);
	console.log("Result of post-increment: " + z);

	// pre-decrement
	// decrease and re-assign
	let decrement = --z;
	console.log("Result of pre-decrement: " + decrement);
	console.log("Result of pre-decrement: " + z);

	// post-decrement
	// re-assign and decrease
	decrement = z--;
	console.log("Result of post-decrement: " + decrement);
	console.log("Result of post-decrement: " + z);

	// Type coercion
	/*
		Type Coercion is the automatic or implicit conversion of values from one data type to another
	*/

	let numA = '10';
	let numB = 12;

	let coercion = numA + numB;
	console.log(coercion); // "1012"

	let numC = 16;
	let numD = 14;
	let nonCoercion = numC + numD
	console.log(nonCoercion);
	console.log(typeof nonCoercion);


	// The boolen "true" is associated with the value of 1 
	let numE = true + 1;
	console.log(numE);

	// the boolean "false is associated with the value of 0
	let numF = false + 1;
	console.log(numF);

	// Comparison Operators

	let juan = 'juan';

	// Equality Operator

	console.log(1 == 1); //true
	console.log(1 == 2); //false
	console.log(1 == '1'); //true
	console.log(0 == false); //true
	console.log('juan' == 'juan'); //true
	console.log('juan' == juan); //true

	// Inequality Operator

	console.log(1 != 1); //false
	console.log(1 != 2); //true
	console.log(1 != '1'); //false
	console.log(0 != false); //false
	console.log('juan' != 'juan'); //false
	console.log('juan' != juan); //false

	// Stric equality Operator (===)

	console.log(1 === 1); 
	console.log(1 === 2); 
	console.log(1 === '1'); 
	console.log(0 === false); 
	console.log('juan' === 'juan');
	console.log('juan' === juan); 

	// Strict Inequality Operator (!==)

	console.log(1 !== 1); 
	console.log(1 !== 2); 
	console.log(1 !== '1'); 
	console.log(0 !== false); 
	console.log('juan' !== 'juan'); 
	console.log('juan' !== juan); 

	// Relational Operators

	let abc = 50;
	let def = 65;

	let isGreaterThan = abc > def; //false
	let isLessThan = abc < def; //true
	let isGTOrEqual = abc >= def; //false
	let isLTOrEqual = abc <= def; //true

	console.log(isGreaterThan);
	console.log(isLessThan);
	console.log(isGTOrEqual);
	console.log(isLTOrEqual);

	let numStr = "30"; //forced coercion to changhe the string into number
	console.log(abc > numStr); //true
	console.log(def <= numStr); //false

	let str = "Twenty";
	console.log (def >= str);

// Logical Operator

let isLegalAge = true;
let isRegistered = false;
// && (AND), || (OR), ! (NOT)

	// && 
		// Return true if all operands are true
	let allRequirmentsMet = isLegalAge && isRegisteredl;
	console.log("Result of AND operator: " + allRequirmentsMet); //false

	// ||
		// Returns true if ONE of the operands are true
	let someRequirmentsMet = isLegalAge && isRegisteredl;
	console.log("Result of OR operator: " + someRequirmentsMet); //true

	// !
		// Returns the opposite value
	let someRequirmentsNotMet = !isRegistered;
	console.log("Result of NOT operator: " + someRequirmentsNotMet); // true






