// CRUD Operations

/*
	-CRUD operations are the hearth of any backend application

	-Mastering the CRUD operations is essential for any deveveloper

	-Mastering the CRUD operations of any language makes us a valuable developer and makes the work easier for us to deal a huge amounts of information.


*/

// to swtich dbs, we use - use <dbName>

// Create - Inser documents
	// Insert one document
		// Syntax:
			// db.collectionName.insertOne({object});

	db.users.insertOne({
		"firstName": "John",
		"lastName": "Smith"

	})

	db.users.insertOne({
		firstName: "Jane",
		lastName: "Doe",
		age: 21,
		contact:{
			phone: "87000",
			email: "janedoe@gmail.com"
		},
		courses:["CSS", "JavaScript", "Python"],
		department: "none"
	})

	// Insert Many
	/*
		Syntax
			db.collectionName.insertMany([{ObjectA}, {objectB}])
	*/

	db.users.insertMany([
			{
				firstName: "Stephen",
				lastName: "Hawking",
				age: 76,
				contact:{
					phone: "87001",
					email: "stephenhawking@gmail.com"
				},
				courses:["Python", "React" , "PHP"],
				department: "none"
			},

			{
				"firstName": "Neil",
				"lastName": "ArmStrong",
				age: "82",
				contact: {
					phone: "87002",
					email: "neilarmstrong@gmail.com"
				},
				courses:["React" , "Laravel", "Sass"],
				department: "none"

			}

		])

// Read - Finding Documents

// Find

	/*
		Syntax:
		db.collectionName.find(); - returns all the documents
		db.collectionName.find({firstName:"Stephen"}) - returns the specified document matches the criteria

	*/
	// Finding a single document
		// leaving the search criteria empty will retrieve ALL  the documents
	db.users.find();

	db.users.find({firstName:"Stephen"})

// Finding document with multiple parameters
	/*
		Syntax: 
			db.collectionName.find({fieldA: valueA, fieldB:valueB})
	*/

	db.users.find({lastName:"ArmStrong", age: "82"})

// Update - Updating a document

	// Updating a single document

		// let's add another document

		db.users.insertOne({
			firstName: "Test",
			lastName: "Test",
			age: 0,
			contact: {
				phone: "00000",
				email: "test@gmail.com"
			},
			courses: [],
			department: "none"
		})

		/*
			Syntax: 
				db.collectionName.updateOne({criteria},{$set:{field:value}});

		*/
		// [$set] operator replaces the value of a field with the specified value
		db.users.updateOne(
				{firstName: "Test"},
				{
					$set:{
						firstName: "Bill",
						lastName: "Gates",
						age: 65,
						contact:{
							phone: "12345678",
							email: "bill@gmail.com"
						},
						courses: ["PHP", "Laravel", "HTML"],
						department: "Operations",
						status: "active"
					}
				}

			)
		db.users.find({firstName: "Bill"})
		// status is inserted automatically

	// updating a multiple documents
		/*
			Syntax:
				db.collectionName.updateMany({criteria},{$set:{field:value}});
		*/

		db.users.updateMany(
				{department: "none"},

				{
					$set:{department: "HR"}
				}

			)


// [Delete]

// create a document to delete

		db.users.insert({
			firstName: "test"
		})

// Deleting a single document
	/*
		Syntax
		db.collectionaName.deleteOne({criteria})
	*/

	db.users.deleteOne({
		firstName: "test"
	})

// Delete Many
	/*
		Be careful when using the deleteMany method. if we do not add a search criteria, it will DELETE ALL DOCUMENTS in a db
		DO NOT USE: databaseName.collectionName({criteria})
	*/

	db.users.deleteMany({
		firstName: "Bill"
	})